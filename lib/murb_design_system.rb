# frozen_string_literal: true

require_relative "murb_design_system/version"

# murb design system module contains the engine that enables asset pipeline to work
module MurbDesignSystem
  class Error < StandardError; end
  # Your code goes here...

  if defined?(Rails)
    class Engine < Rails::Engine
    end
  end
end
